# Docker image of qemu-system-riscv based on EulixOS

A container image within qemu-system-risv64.

## Usage

You can run this container with follow command on any machine of x86_64(amd64) or aarch64(arm64).

`docker run -it isrc.iscas.ac.cn/eulixos/docker/qemu:riscv`

### Run with openEuler image

openEuler is the main one of EulixOS's upstream community, which has a preview version of 64-bit RISC-V image. This container has a built-in openEuler RISC-V image,  which is from https://repo.openeuler.org/openEuler-preview/RISC-V/Image/ .

You can run this container with follow command on any machine of x86_64(amd64) or aarch64(arm64). And you will directly login into console of openEuler RISC-V virtual machine after vm startup finish.

`docker run -it isrc.iscas.ac.cn/eulixos/docker/qemu:riscv-oe`

If you want run a virtual machine backgroud, you can run the following command.

`docker run -d -p 10022:22 isrc.iscas.ac.cn/eulixos/docker/qemu:riscv-oe`

