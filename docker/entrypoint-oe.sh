#!/bin/bash

set -e

export VM_SSH_PORT=22
export VM_SSH_USERNAME=root
export VM_SSH_PASSWORD=""
export VM_MEMORY=4G
export VM_CPU_SMP=8
export DOCKER_MAP_SSH_PORT=22
export QEMU_LOG=/qemu.log
export MAX_RETRY_TIMES=60

if [ -f /vm.config ] ; then
    . /vm.config
fi

run-qemu()
{
    echo ----------------------
    echo Starting qemu-system-riscv64 "$@"
    qemu-system-riscv64 "$@" 2>$QEMU_LOG >$QEMU_LOG
}

run-qemu \
  -nographic -machine virt \
  -smp $VM_CPU_SMP -m $VM_MEMORY \
  -kernel /fw_payload_oe_docker.elf \
  -drive file=/staging.qcow2,format=qcow2,id=hd0 \
  -object rng-random,filename=/dev/urandom,id=rng0 \
  -device virtio-rng-device,rng=rng0 \
  -device virtio-blk-device,drive=hd0 \
  -device virtio-net-device,netdev=usernet \
  -netdev user,id=usernet,hostfwd=tcp::$DOCKER_MAP_SSH_PORT-:$VM_SSH_PORT \
  -append 'root=/dev/vda1 rw console=ttyS0 systemd.default_timeout_start_sec=600 selinux=0 highres=off mem=4096M earlycon' &

sleep 3

res=0
sshconnect=0
retry_times=$MAX_RETRY_TIMES

while [ "$retry_times" -gt 0 ]
do
    echo
    echo --------------------
    tail -n 10 $QEMU_LOG
    echo ------ last 10 lines of log: $QEMU_LOG
    echo Waiting VM to finish startup...
    nc -zv localhost $DOCKER_MAP_SSH_PORT 2>/dev/null >/dev/null && res=1 || res=0
    if [ "x$res" == "x1" ] ; then
        set +e
        sshpass -p 'WRONG_PASSWORD' ssh -o ConnectTimeout=3 -p $DOCKER_MAP_SSH_PORT -o StrictHostKeyChecking=no -T $VM_SSH_USERNAME@localhost 2>/dev/null >/dev/null
        res=$?
        set -e
        if [ "x$res" == "x0" ] || [ "x$res" == "x5" ] ; then
            sshconnect=1
            echo VM finish startup and could be connected by ssh now.
            break
        fi
    fi
    retry_times=$(($retry_times-1))
    sleep 7
done

TOEXIT=n

while [ "x$TOEXIT" != "xy" ]
do
    echo
    echo Connecting by ssh -p $DOCKER_MAP_SSH_PORT $VM_SSH_USERNAME@localhost...
    echo
    set +e
    SSHPASS_WITHPASSWORD="sshpass -p $VM_SSH_PASSWORD"
    if [ "x$VM_SSH_PASSWORD" != "x" ] ; then
        sshpass -p $VM_SSH_PASSWORD ssh -o ConnectTimeout=60 -p $DOCKER_MAP_SSH_PORT -o StrictHostKeyChecking=no $VM_SSH_USERNAME@localhost
    else
        ssh -o ConnectTimeout=60 -p $DOCKER_MAP_SSH_PORT -o StrictHostKeyChecking=no $VM_SSH_USERNAME@localhost
    fi
    echo --------------------
    echo It looks like that you can not login VM or exit from vm just now.
    echo you can choose to login again or exit.
    echo If you exit, the qemu riscv virtual machine will also be stopped.
    echo 
    read -p "Do you want to try again?(Y/N)" answer
    if [ "x$answer" == "xY" ] || [ "x$answer" == "xy" ] || [ "x$answer" == "x" ] ; then
        TOEXIT=n
    else
        TOEXIT=y
        echo Exit.
    fi
done

