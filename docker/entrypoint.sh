#!/bin/bash

set -e

echo --------------------------
echo Starting qemu-system-riscv64 "$@"

qemu-system-riscv64 "$@"

